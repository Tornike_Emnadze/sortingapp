package com.epam.rd.autotasks;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class AppTest {

    Sorting sorting = new Sorting();

    @Test
    public void testNullCase() {
        int[] arr = null;
            assertThrows(IllegalArgumentException.class, ()-> sorting.sort(arr));
    }

    @Test
    public void testEmptyCase() {
        int[] arr = {};
        sorting.sort(arr);
        assertArrayEquals(arr, new int[]{});
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] arr = {5};
        sorting.sort(arr);
        assertArrayEquals(arr, new int[]{5});
    }

    @Test
    public void testSortedArraysCase() {
        int[] arr = {1, 2, 3, 4, 5};
        int[] expected = {1, 2, 3, 4, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testOtherCases() {
        int[] arr = {4, 2, 1, 5, 3};
        int[] expected = {1, 2, 3, 4, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testEmptyArrayCase() {
        int[] arr = {};
        sorting.sort(arr);
        assertArrayEquals(arr, new int[]{});
    }

    @Test
    public void testReverseSortedArrayCase() {
        int[] arr = {5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testSameElementsArrayCase() {
        int[] arr = {5, 5, 5, 5, 5};
        int[] expected = {5, 5, 5, 5, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testLargeRandomArrayCase() {
        int[] arr = {9, 2, 5, 1, 7, 3, 8, 4, 6};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testNegativeElementsArrayCase() {
        int[] arr = {-5, -2, -1, -4, -3};
        int[] expected = {-5, -4, -3, -2, -1};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testDuplicateElementsArrayCase() {
        int[] arr = {4, 2, 1, 5, 3, 2, 4};
        int[] expected = {1, 2, 2, 3, 4, 4, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testDescendingOrderArrayCase() {
        int[] arr = {5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }

    @Test
    public void testLargeNumberOfElementsCase() {
        int[] arr = {9, 3, 5, 1, 7, 4, 8, 2, 6, 10, 12, 15, 13, 11, 14};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        sorting.sort(arr);
        assertArrayEquals(arr, expected);
    }
}