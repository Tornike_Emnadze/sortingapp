package com.epam.rd.autotasks;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sorting extends JFrame {
    private List<Integer> numbers;
    private JTextField numberField;
    private JTextArea sortedArea;
    private DefaultListModel<Integer> numberListModel;
    private JList<Integer> numberList;

    public Sorting() {
        setTitle("Sorting Algorithm");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(500, 200));

        JPanel inputPanel = new JPanel(new FlowLayout());
        numberField = new JTextField(10);
        JButton addButton = new JButton("Add Number");
        addButton.addActionListener(new AddNumberActionListener());
        inputPanel.add(numberField);
        inputPanel.add(addButton);

        numberListModel = new DefaultListModel<>();
        numberList = new JList<>(numberListModel);
        JScrollPane listScrollPane = new JScrollPane(numberList);

        sortedArea = new JTextArea(5, 20);
        sortedArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(sortedArea);

        JButton sortButton = new JButton("Sort");
        sortButton.addActionListener(new SortActionListener());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(sortButton);

        add(inputPanel, BorderLayout.NORTH);
        add(listScrollPane, BorderLayout.WEST);
        add(scrollPane, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
    }

    private class AddNumberActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                int number = Integer.parseInt(numberField.getText());
                if (numbers == null) {
                    numbers = new ArrayList<>();
                }
                numbers.add(number);
                numberListModel.addElement(number);
                numberField.setText("");
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(Sorting.this, "Invalid input. Please enter a valid integer.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class SortActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (numbers != null) {
                int[] array = new int[numbers.size()];
                for (int i = 0; i < numbers.size(); i++) {
                    array[i] = numbers.get(i);
                }

                sort(array);

                sortedArea.setText("Sorted array: " + Arrays.toString(array));
            }
        }
    }

    public void sort(int[] array) {
        if (array == null)
            throw new IllegalArgumentException("Array can't be null");

        if (array.length > 0) {
            int n = array.length;
            int temp = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < (n - i); j++) {
                    if (array[j - 1] > array[j]) {
                        // swap elements
                        temp = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Sorting sorting = new Sorting();
            sorting.setVisible(true);
        });
    }
}
